cmake_minimum_required(VERSION 3.24)
project(command_event)

set(CMAKE_CXX_STANDARD 17)

add_executable(server server.cpp)
